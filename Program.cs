﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SudokuSolver2
{

    class Program
    {
        // Specify the upper left coordinates of each block
        public static (int, int)[] blockCoords = { (0, 0), (3, 0), (6, 0), (0, 3), (3, 3), (6, 3), (0, 6), (3, 6), (6, 6) };

        // Contains list of the numbers that are fixed in each block [block, value]        
        public static bool[,] blockFixedNumbers = new bool[9, 9];

        // Contains the coordinates of the numbers that are fixed in each block
        public static bool[,] fixedCoords = new bool[9, 9];

        // Contains the sudoku
        public static int[,] grid = new int[9, 9];

        static void Main(string[] args)
        {
            InitializeGrid();
            
            Print();

            //CBT();

            CBTwithFC();

            Print();
            
            Console.ReadKey();
        }

        // Cock and Ball Torture with Five Cocks
        public static void CBTwithFC()
        {
            // x, y, number
            FCState[,,] domains = new FCState [9, 9, 9];

            // Domain initialization
            for (int x = 0; x < 9; x++)
            {
                for (int y = 0; y < 9; y++)
                {
                    if (fixedCoords[x, y])
                    {
                        int num = grid[x, y];

                        int blockX = x / 3;
                        int blockY = y / 3;

                        for (int l = 0; l < 9; l++)
                        {
                            domains[l, y, num] = FCState.unavailable; // set numbers in the same row to unavailable
                            domains[x, l, num] = FCState.unavailable; // set numbers in the same column to unavailable

                            domains[x, y, l] = FCState.unavailable;

                            int curryX = 3 * blockX + (l % 3);
                            int curryY = 3 * blockY + (l / 3);

                            domains[curryX, curryY, num] = FCState.unavailable; // set numbers in the same block to unavailable
                        }

                        continue;
                    }

                    for (int num = 0; num < 9; num++)
                    {
                        if (domains[x, y, num] != FCState.uninitialized)
                        {
                            continue;
                        }                        

                        domains[x, y, num] = FCState.available;
                     
                    }
                }
            }

            Stack<FCAssignment> actions = new Stack<FCAssignment>();            
            Stack<Assignment> stack = new Stack<Assignment>();
            Stack<FCAssignment> removals = new Stack<FCAssignment>();

            Assignment firstAssignment = new Assignment() { X = 0, Y = 0, num = 0 };

            stack.Push(firstAssignment);

            int iterations = 0;

            while (stack.Count > 0)
            {
                iterations++;
                Assignment assignment = stack.Pop();

                if (fixedCoords[assignment.X, assignment.Y])
                {
                    bool nextRow = assignment.X == 8;
                    if (nextRow && assignment.Y == 8)
                    {
                        return;
                    }
                    int newX = nextRow ? 0 : assignment.X + 1;
                    int newY = nextRow ? assignment.Y + 1 : assignment.Y;
                    stack.Push(new Assignment() { X = newX, Y = newY, num = 0 });
                    continue;
                }

                grid[assignment.X, assignment.Y] = assignment.num;

                bool success = false;
                bool attempted = false;

                if (domains[assignment.X, assignment.Y, assignment.num] == FCState.available) // proceed to next coordinate
                {
                    attempted = true;
                    success = true;
                    bool shouldBacktrack = true;
                    actions.Push(new FCAssignment(assignment, iterations));

                    int blockX = assignment.X / 3;
                    int blockY = assignment.Y / 3;

                    for (int l = 0; l < 9; l++)
                    {
                        shouldBacktrack = true;
                        if (domains[l, assignment.Y, assignment.num] != FCState.unavailable && l != assignment.X) // set numbers in the same row to unavailable
                        {
                            domains[l, assignment.Y, assignment.num] = FCState.unavailable; 
                            removals.Push(new FCAssignment() { X = l, Y = assignment.Y, num = assignment.num, depth = iterations });                            
                            for (int m = 0; m < 9; m++)
                            {
                                if (domains[l, assignment.Y, m] == FCState.available)
                                {
                                    shouldBacktrack = false;
                                }
                            }
                            if (shouldBacktrack)
                            {
                                success = false;
                                break;
                            }
                        }
                        shouldBacktrack = true;
                        if (domains[assignment.X, l, assignment.num] != FCState.unavailable && l != assignment.Y) // set numbers in the same column to unavailable
                        {
                            domains[assignment.X, l, assignment.num] = FCState.unavailable; 
                            removals.Push(new FCAssignment() { X = assignment.X, Y = l, num = assignment.num, depth = iterations });
                            for (int m = 0; m < 9; m++)
                            {
                                if (domains[assignment.X, l, m] == FCState.available)
                                {
                                    shouldBacktrack = false;
                                }
                            }
                            if (shouldBacktrack)
                            {
                                success = false;
                                break;
                            }
                        }
                        if (domains[assignment.X, assignment.Y, l] != FCState.unavailable) // set numbers in the same cell to unavailable
                        {
                            domains[assignment.X, assignment.Y, l] = FCState.unavailable; 
                            removals.Push(new FCAssignment() { X = assignment.X, Y = assignment.Y, num = l, depth = iterations });
                        }

                        int curryX = 3 * blockX + (l % 3);
                        int curryY = 3 * blockY + (l / 3);

                        shouldBacktrack = true;
                        if (domains[curryX, curryY, assignment.num] != FCState.unavailable && !(curryX == assignment.X && curryY == assignment.Y)) // set numbers in the same block to unavailable
                        {
                            domains[curryX, curryY, assignment.num] = FCState.unavailable; 
                            removals.Push(new FCAssignment() { X = curryX, Y = curryY, num = assignment.num, depth = iterations });
                            for (int m = 0; m < 9; m++)
                            {
                                if (domains[curryX, curryY, m] == FCState.available)
                                {
                                    shouldBacktrack = false;
                                }
                            }
                            if (shouldBacktrack)
                            {
                                success = false;
                                break;
                            }
                        }
                    }

                    if (success)
                    {
                        bool nextRow = assignment.X == 8;
                        if (nextRow && assignment.Y == 8)
                        {
                            return;
                        }
                        int newX = nextRow ? 0 : assignment.X + 1;
                        int newY = nextRow ? assignment.Y + 1 : assignment.Y;
                        stack.Push(new Assignment() { X = newX, Y = newY, num = 0 });
                    }
                }

                if (!success)
                {
                    if (assignment.num == 8 || attempted) // backtrack
                    {
                        grid[assignment.X, assignment.Y] = -1;

                        FCAssignment lastAction;
                        do
                        {
                            lastAction = actions.Pop();
                            grid[lastAction.X, lastAction.Y] = -1; // empty this cell
                            FCAssignment lastRemoval;
                            do
                            {
                                lastRemoval = removals.Pop();
                                if (lastRemoval.depth == lastAction.depth)
                                {
                                    domains[lastRemoval.X, lastRemoval.Y, lastRemoval.num] = FCState.available;

                                }
                                else
                                {
                                    removals.Push(lastRemoval); // restore removal that didn't belong to this action
                                }
                            } while (lastRemoval.depth == lastAction.depth && removals.Count > 0);
                        } while (lastAction.num == 8);

                        lastAction.num++;
                        stack.Push(new Assignment(lastAction));
                    }
                    else
                    {
                        // try the next number at this coordinate
                        stack.Push(new Assignment() { X = assignment.X, Y = assignment.Y, num = assignment.num + 1 });
                    }
                }
            }
        }

        // Cock & Ball Torture
        public static void CBT()
        {
            Stack<Assignment> actions = new Stack<Assignment>();
            Stack<Assignment> stack = new Stack<Assignment>();

            Assignment firstAssignment = new Assignment() { X = 0, Y = 0, num = 0 };

            stack.Push(firstAssignment);

            int iterations = 0;

            while (stack.Count > 0)
            {
                iterations++;
                Assignment assignment = stack.Pop();
                
                if (fixedCoords[assignment.X, assignment.Y])
                {
                    bool nextRow = assignment.X == 8;
                    if (nextRow && assignment.Y == 8)
                    {
                        return;
                    }
                    int newX = nextRow ? 0 : assignment.X + 1;
                    int newY = nextRow ? assignment.Y + 1 : assignment.Y;
                    stack.Push(new Assignment() { X = newX, Y = newY, num = 0 });
                    continue;
                }
                
                grid[assignment.X, assignment.Y] = assignment.num;

                if (CheckRow(assignment.Y) && CheckColumn(assignment.X) && CheckBlock(assignment.X, assignment.Y)) // proceed to next coordinate
                {
                    actions.Push(assignment);
                    
                    bool nextRow = assignment.X == 8;
                    if (nextRow && assignment.Y == 8)
                    {
                        return;
                    }
                    int newX = nextRow ? 0 : assignment.X + 1;
                    int newY = nextRow ? assignment.Y + 1 : assignment.Y;
                    stack.Push(new Assignment() { X = newX, Y = newY, num = 0});
                } 
                else
                {
                    if (assignment.num == 8) // backtrack
                    {
                        grid[assignment.X, assignment.Y] = -1;
                        Assignment lastAction;
                        do
                        {
                            lastAction = actions.Pop();
                            grid[lastAction.X, lastAction.Y] = -1; // empty this cell
                        } while (lastAction.num == 8);

                        lastAction.num++;
                        stack.Push(lastAction);
                    } else
                    {
                        // try the next number at this coordinate
                        stack.Push(new Assignment() { X = assignment.X, Y = assignment.Y, num = assignment.num + 1 });
                    }
                }
            }
        }

        // Checks if a row is still legal
        public static bool CheckRow(int y)
        {
            bool[] numbers = new bool[9];
            for (int x = 0; x < 9; x++)
            {
                int number = grid[x, y];
                if (number == -1)
                {
                    continue;
                }
                if (numbers[number])
                {
                    return false;
                }
                numbers[number] = true;
            }

            return true;
        }

        // Checks if a column is still legal
        public static bool CheckColumn(int x)
        {
            bool[] numbers = new bool[9];
            for (int y = 0; y < 9; y++)
            {
                int number = grid[x, y];
                if (number == -1)
                {
                    continue;
                }
                if (numbers[number])
                {
                    return false;
                }
                numbers[number] = true;
            }

            return true;
        }

        // Checks if a block is stil legal
        public static bool CheckBlock(int x, int y)
        {
            int blockX = x / 3;
            int blockY = y / 3;

            bool[] numbers = new bool[9];

            int curryX;
            int curryY;

            for (int i = 0; i < 9; i++)
            {
                curryX = 3 * blockX + (i % 3);
                curryY = 3 * blockY + (i / 3);

                int number = grid[curryX, curryY];
                if (number == -1)
                {
                    continue;
                }
                if (numbers[number])
                {
                    return false;
                }
                numbers[number] = true;
            }

            return true;
        }

        // Prints the sudoku grid
        public static void Print()
        {
            for (int y = 0; y < 9; y++)
            {
                if (y == 3 || y == 6)
                {
                    for (int l = 0; l < 11; l++)
                    {
                        Console.Write('-');
                    }
                    Console.WriteLine();
                }

                for (int x = 0; x < 9; x++)
                {
                    if (x == 3 || x == 6)
                    {
                        Console.Write("|");
                    }

                    Console.Write(grid[x, y] + 1);

                }
                Console.WriteLine();
            }

            Console.WriteLine();
        }

        public static string[] grids = {
            "0 0 3 0 2 0 6 0 0 9 0 0 3 0 5 0 0 1 0 0 1 8 0 6 4 0 0 0 0 8 1 0 2 9 0 0 7 0 0 0 0 0 0 0 8 0 0 6 7 0 8 2 0 0 0 0 2 6 0 9 5 0 0 8 0 0 2 0 3 0 0 9 0 0 5 0 1 0 3 0 0",
            "2 0 0 0 8 0 3 0 0 0 6 0 0 7 0 0 8 4 0 3 0 5 0 0 2 0 9 0 0 0 1 0 5 4 0 8 0 0 0 0 0 0 0 0 0 4 0 2 7 0 6 0 0 0 3 0 1 0 0 7 0 4 0 7 2 0 0 4 0 0 6 0 0 0 4 0 1 0 0 0 3",
            "0 0 0 0 0 0 9 0 7 0 0 0 4 2 0 1 8 0 0 0 0 7 0 5 0 2 6 1 0 0 9 0 4 0 0 0 0 5 0 0 0 0 0 4 0 0 0 0 5 0 7 0 0 9 9 2 0 1 0 8 0 0 0 0 3 4 0 5 9 0 0 0 5 0 7 0 0 0 0 0 0",
            "0 3 0 0 5 0 0 4 0 0 0 8 0 1 0 5 0 0 4 6 0 0 0 0 0 1 2 0 7 0 5 0 2 0 8 0 0 0 0 6 0 3 0 0 0 0 4 0 1 0 9 0 3 0 2 5 0 0 0 0 0 9 8 0 0 1 0 2 0 6 0 0 0 8 0 0 6 0 0 2 0",
            "0 2 0 8 1 0 7 4 0 7 0 0 0 0 3 1 0 0 0 9 0 0 0 2 8 0 5 0 0 9 0 4 0 0 8 7 4 0 0 2 0 8 0 0 3 1 6 0 0 3 0 2 0 0 3 0 2 7 0 0 0 6 0 0 0 5 6 0 0 0 0 8 0 7 6 0 5 1 0 9 0"
        };

        // Parses the grid and keeps track of the fixed numbers
        public static void InitializeGrid()
        {
            string[] input = grids[4].Split();

            for (int c = 0; c < blockCoords.Length; c++)
            {
                int baseX = blockCoords[c].Item1;
                int baseY = blockCoords[c].Item2;

                for (int y = baseY; y < baseY + 3; y++)
                {
                    for (int x = baseX; x < baseX + 3; x++)
                    {
                        grid[x, y] = int.Parse(input[y * 9 + x]) - 1;

                        if (grid[x, y] > -1)
                        {
                            fixedCoords[x, y] = true;
                            blockFixedNumbers[c, grid[x, y]] = true;
                        }
                    }
                }
            }
        }

        // Fills the empty places in the Sudoku to create a valid problem state
        public static void FillGrid()
        {
            for (int c = 0; c < blockCoords.Length; c++)
            {
                int x = blockCoords[c].Item1;
                int y = blockCoords[c].Item2;

                int number = 0;

                for (int i = x; i < x + 3; i++)
                {
                    for (int j = y; j < y + 3; j++)
                    {
                        if (grid[i, j] == -1)
                        {
                            while (blockFixedNumbers[c, number])
                            {
                                number++;
                            }

                            grid[i, j] = number;
                            number++;
                        }
                    }
                }
            }
        }

        
    }
    public enum FCState
    {
        uninitialized,
        available,
        unavailable,
    }

    struct Assignment
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int num { get; set; }

        public Assignment(FCAssignment ass)
        {
            this.X = ass.X;
            this.Y = ass.Y;
            this.num = ass.num;
        }
    }

    struct FCAssignment
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int num { get; set; }
        public int depth { get; set; }

        public FCAssignment(Assignment ass, int depth)
        {
            this.X = ass.X;
            this.Y = ass.Y;
            this.num = ass.num;
            this.depth = depth;
        }
    }
}
